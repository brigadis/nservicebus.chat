﻿using System;
using NServiceBus;
using StructureMap;

namespace Chat.Subscriber1
{
    public class EndpointConfig : IConfigureThisEndpoint,AsA_Server,IWantCustomInitialization
    {
        public void Init()
        {
            ObjectFactory.Initialize(x => x.Scan(i => i.TheCallingAssembly()));
            Configure.With()
                     .StructureMapBuilder(ObjectFactory.Container)
                     .DefiningEventsAs(t => t.Namespace != null && t.Namespace.EndsWith("Contracts"));
        }
    }
}
