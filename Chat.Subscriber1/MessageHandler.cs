﻿using System;
using Chat.Contracts;
using NServiceBus;

namespace Chat.Subscriber1
{
    public class MessageHandler : IMessageHandler<Message>
    {
        public void Handle(Message message)
        {
            Console.WriteLine("Message received:  " + message.Text);
        }
    }
}