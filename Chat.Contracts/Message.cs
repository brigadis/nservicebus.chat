﻿using System;
using NServiceBus;

namespace Chat.Contracts
{
    public class Message : IEvent
    {
        public string Text { get; set; }
        public DateTime Sent { get; set; }
    }
}