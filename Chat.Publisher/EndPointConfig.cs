﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chat.Contracts;
using NServiceBus;
using StructureMap;

namespace Chat.Publisher
{
    public class EndPointConfig : IConfigureThisEndpoint, AsA_Publisher, IWantCustomInitialization, IWantToRunAtStartup
    {
        public void Init()
        {
            ObjectFactory.Initialize(x => x.Scan(i => i.TheCallingAssembly()));
            Configure.With()
                     .StructureMapBuilder(ObjectFactory.Container)
                     .DefiningEventsAs(t => t.Namespace != null && t.Namespace.EndsWith("Contracts"));
        }

        public void Run()
        {
            var bus = ObjectFactory.GetInstance<IBus>();
            bus.Publish(new Message
                            {
                                Sent = DateTime.Now,
                                Text = "Woho"
                            });
        }

        public void SendMessage(string messsage)
        {
            var bus = ObjectFactory.GetInstance<IBus>();
            bus.Publish(new Message
            {
                Sent = DateTime.Now,
                Text = "Woho"
            });
        }

        public void Stop()
        {

        }
    }
}
