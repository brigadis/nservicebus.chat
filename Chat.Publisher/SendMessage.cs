﻿using System;
using Chat.Contracts;
using NServiceBus;
using StructureMap;

namespace Chat.Publisher
{
    public interface ISendMessage
    {
        void Send(string message);
    }

    public class SendMessage : ISendMessage
    {
        private readonly IBus _bus;

        public SendMessage(IBus bus)
        {
            if (bus == null) throw new ArgumentNullException("bus");
            _bus = bus;
        }

        public void Send(string message)
        {
            _bus.Publish(new Message
            {
                Sent = DateTime.Now,
                Text = "Woho"
            });
        }
    }
}